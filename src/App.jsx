import React from 'react';
import { hot } from 'react-hot-loader';

import Bookings from './components/Bookings';

function App() {
  return (
    <Bookings />
  );
}

export default hot(module)(App);
